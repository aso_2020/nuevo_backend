const express = require("express");
const router = express.Router();
const dataAccess = require("../dataAccess/dataAccess");
const virusTotalModule = require("../virusTotalModule/virusTotalModule");

const multer = require("multer");
const upload = multer(); // Para datos tipo multipart/form-data

router.get("/", async (req, res) => {
  res.send({ code: 200, message: "hola" });
});

router.post("/", upload.single("archivo"), (req, res) => {
  console.log("post");
  console.log(req.file);
  virusTotalModule.getFileId(req.file);

  res.sendStatus(200);
});

module.exports = router;
