const axios = require("axios");
const FormData = require("form-data");
const fs = require("fs");
const headers = {
  "x-apikey": process.env.VIRUSTOTALTOKEN,
  "Content-Type": "multipart/form-data",
};

const getFileId = (file) => {
  const formData = new FormData();
  console.log(file);
  formData.append("file", JSON.stringify(file));

  console.log(formData);

  axios
    .post("https://www.virustotal.com/api/v3/files", formData, {
      headers: {
        "x-apikey": process.env.VIRUSTOTALTOKEN,
      },
    })
    .then((res) => {
      let fileId = res.data.data.id;
      console.log("fileId tiene: ", fileId);

      /*         axios.get(`https://www.virustotal.com/api/v3/analyses/${fileId}`, { headers }).then((response) => {
                        console.log('respuesta final tiene: ', response);
                        axios.post('localhost:4000/api/analyze', response)
                            .then(resultadoFinal => { console.log('lo que vuelve del nuevo back: ', resultadoFinal) })
                    }).catch(err => {
                        console.log(err);
                    }); */
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports = { getFileId };
