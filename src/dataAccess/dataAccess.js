const FileToAnalize = require('../models/FileToAnalize')

checkFileExists = (fileId) => {
    return FileToAnalize.find({ "data.id": fileId })
}

module.exports = { checkFileExists }